function gcd(a, b) {
  if (!a)
    return b;
  return gcd(b % a, a);
}

function phi(n) {
  let result = 1;
  for (let i = 2; i < n; i++)
    if (gcd(i, n) === 1)
      result++;
  return result;
}

